package com.vht.portsipexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.vht.portsipexample.ui.CallActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnCall.setOnClickListener {
            edtPhone.text.toString().takeIf { it.isNotEmpty() }?.let {
                startActivity(
                    CallActivity.launchOutgoingCall(this, it)
                )
            } ?: kotlin.run {
                Toast.makeText(this, "Empty phone", Toast.LENGTH_SHORT).show()
            }
        }
    }
}