package com.vht.portsipexample.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.vht.portsipexample.service.PortSipService.Companion.EXTRA_IS_ONLINE
import com.vht.portsipexample.util.CallManager
import com.vht.portsipexample.util.Session

class PortSipBroadcast : BroadcastReceiver() {
    var portSipDelegate: PortSipDelegate? = null
    var portSipDelegateState: PortSipStateDelegate? = null

    override fun onReceive(context: Context?, intent: Intent?) {
        intent?.also {
            when (it.action ?: "") {
                PortSipService.REGISTER_CHANGE_ACTION -> {
                    val isOnline = it.getBooleanExtra(EXTRA_IS_ONLINE, false)
                    portSipDelegateState?.onRegisteringStateChanged(isOnline)
                }

                PortSipService.CALL_CHANGE_ACTION -> {
                    // get current sessionId from Intent
                    val sessionId = it.getLongExtra(
                        PortSipService.EXTRA_CALL_SEESIONID,
                        Session.INVALID_SESSION_ID.toLong()
                    )

                    // find the associated session with provided id
                    val session = CallManager.instance().findSessionBySessionID(sessionId)

                    if (session != null) {
                        // notify event with session
                        portSipDelegate?.onCallStateChanged(session, session.state)
                    }

                }
                PortSipService.PRESENCE_CHANGE_ACTION -> Unit
                else -> Log.d(LOG, "not a valid action")
            }
        }
    }

    companion object {
        const val LOG = "PortSipBroadcast"
    }

}

interface PortSipDelegate {
    fun onCallStateChanged(session: Session?, state: Session.CALL_STATE_FLAG)
}

interface PortSipStateDelegate {
    fun onRegisteringStateChanged(isOnline: Boolean)
}
