package com.vht.portsipexample

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Build
import com.portsip.PortSipSdk
import com.vht.portsipexample.service.PortSipService

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        configUser()
        initializePortSip()
    }

    private fun initializePortSip() {
        val onLineIntent = Intent(applicationContext, PortSipService::class.java)
        onLineIntent.action = PortSipService.ACTION_SIP_REGIEST
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            applicationContext.startForegroundService(onLineIntent)
        } else {
            applicationContext.startService(onLineIntent)
        }
    }

    fun disconnect() {
        val onLineIntent = Intent(applicationContext, PortSipService::class.java)
        onLineIntent.action = PortSipService.ACTION_SIP_UNREGIEST
        applicationContext.startService(onLineIntent)
    }

    private fun configUser() {
        this.getSharedPreferences("portsip.config", Context.MODE_PRIVATE).edit().also {
            it.putString(PortSipService.USER_NAME, "200")
            it.putString(PortSipService.USER_PWD, "Abcd1234")
            it.putString(PortSipService.SVR_HOST, "portsip.vht.com.vn")
            it.putString(PortSipService.SVR_PORT, "5060")
            it.putString(PortSipService.USER_DISPALYNAME, "200")
            it.putString(PortSipService.USER_DOMAIN, "mt1.vht.com.vn")
            it.putString(PortSipService.USER_AUTHNAME, "200")
            it.putString(PortSipService.STUN_HOST, "")
            it.putString(PortSipService.STUN_PORT, "3478")
            it.apply()
        }
    }

    companion object {
        val portSipSDK = PortSipSdk()
    }
}