package com.vht.portsipexample.ui

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import com.portsip.PortSipEnumDefine
import com.portsip.PortSipSdk
import com.vht.portsipexample.MainApplication
import com.vht.portsipexample.R
import com.vht.portsipexample.service.PortSipBroadcast
import com.vht.portsipexample.service.PortSipDelegate
import com.vht.portsipexample.service.PortSipService
import com.vht.portsipexample.util.CallManager
import com.vht.portsipexample.util.Ring
import com.vht.portsipexample.util.Session
import kotlinx.android.synthetic.main.activity_call_out.*

open class CallActivity : AppCompatActivity(), PortSipDelegate {
    private val portSipSdk: PortSipSdk by lazy {
        MainApplication.portSipSDK
    }
    private var sessionID = -1
    private var callOutPhone = ""
    private var typeCall: CallType = CallType.TYPE_OUTGOING_CALL
    private val mPortSipBroadcast: PortSipBroadcast by lazy {
        PortSipBroadcast()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call_out)
        initializeListener()

        mPortSipBroadcast.portSipDelegate = this
        val filter = IntentFilter()
        filter.addAction(PortSipService.REGISTER_CHANGE_ACTION)
        filter.addAction(PortSipService.CALL_CHANGE_ACTION)
        filter.addAction(PortSipService.PRESENCE_CHANGE_ACTION)
        registerReceiver(mPortSipBroadcast, filter)

        callOutPhone = intent?.getStringExtra(PARAM_OUT_NUMBER) ?: ""
        sessionID = (intent?.getStringExtra(PARAM_SESSION_ID) ?: "-1").toInt()
        typeCall = CallType.fromValue(intent?.getIntExtra(PARAM_CALL_TYPE, -1) ?: -1)
        performUIByType(typeCall)
    }

    override fun onResume() {
        super.onResume()
        if (typeCall == CallType.TYPE_OUTGOING_CALL && callOutPhone.isNotEmpty() && CallManager.instance().currentSession.state != Session.CALL_STATE_FLAG.CONNECTED) {
            startCalling(callOutPhone)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mPortSipBroadcast)
    }

    private fun initializeListener() {
        btn_end_call_1.setOnClickListener {
            CallManager.instance().currentSession?.let { currentLine ->
                Ring.getInstance(this).stop()
                when (currentLine.state) {
                    Session.CALL_STATE_FLAG.INCOMING -> {
                        portSipSdk.rejectRefer(currentLine.sessionID)
                        Log.d(LOG, currentLine.lineName.toString() + ": Rejected call")
                    }

                    Session.CALL_STATE_FLAG.CONNECTED,
                    Session.CALL_STATE_FLAG.TRYING -> {
                        portSipSdk.hangUp(currentLine.sessionID)
                        Log.d(LOG, currentLine.lineName.toString() + ": Hang up")
                    }
                    else -> Unit
                }
                currentLine.reset()

                onCallStateChanged(currentLine, Session.CALL_STATE_FLAG.CLOSED)
            }
        }
        btn_speaker.setOnClickListener {
            updateSpeakerState()
        }
        btnMute.setOnClickListener {
            updateMuteState()
        }
        btnHold.setOnClickListener {
            updateHoldState()
        }
        btn_accept.setOnClickListener {
            CallManager.instance().currentSession?.let { currentLine ->
                if (currentLine.state != Session.CALL_STATE_FLAG.INCOMING) {
                    Log.d(LOG, "No incoming call on current line, please switch a line.")
                    return@let
                }
                currentLine.state = Session.CALL_STATE_FLAG.CONNECTED

                portSipSdk.answerCall(currentLine.sessionID, false) //answer call

            }
        }
        btn_decline.setOnClickListener {
            performUIByCallState(Session.CALL_STATE_FLAG.CLOSED)
            setViewsEnable(false, btn_accept)
            setViewsEnable(false, btn_accept, btn_decline)
            CallManager.instance().currentSession?.let { currentLine ->
                if (currentLine.state == Session.CALL_STATE_FLAG.INCOMING) {
                    portSipSdk.rejectCall(currentLine.sessionID, 486)//486 mean busy
                    currentLine.reset()
                    Ring.getInstance(this).stop()
                    Log.d(LOG, currentLine.lineName.toString() + ": Rejected call")
                    return@let
                }
                btnForward.setOnClickListener {
//                    transferCall()
                    Toast.makeText(this, "transfer coming soon", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun startCalling(toPhone: String) {
        val currentLine: Session = CallManager.instance().currentSession

        if (!currentLine.isIdle()) {
            //"Current line is busy now, please switch a line.")
            return
        }

        // Ensure that we have been added one audio codec at least
        if (portSipSdk.isAudioCodecEmpty) {
            //"Audio Codec Empty,add audio codec at first
            return
        }

        // Usually for 3PCC need to make call without SDP
        val sessionId: Long = portSipSdk.call(toPhone, true, false)
        if (sessionId <= 0) {
            //Call failure
            performUIByCallState(Session.CALL_STATE_FLAG.FAILED)
            return
        }

        //default send video
        portSipSdk.addSipMessageHeader(sessionId, "ALL", 3, "order_id", "This is order is")
        portSipSdk.sendVideo(sessionId, true)

        currentLine.remote = toPhone
        currentLine.sessionID = sessionId
        currentLine.state = Session.CALL_STATE_FLAG.TRYING
        currentLine.hasVideo = false
        onCallStateChanged(currentLine, Session.CALL_STATE_FLAG.TRYING)
    }

    fun onTransferCall(phone: String) {
        CallManager.instance().currentSession?.let { currentLine ->
            if (currentLine.state != Session.CALL_STATE_FLAG.CONNECTED) {
                //"current line must be connected
                return
            }

            val rt: Int = portSipSdk.refer(currentLine.sessionID, phone)
            if (rt != 0) {
                //failed to transfer
            } else {
                //success transfered
            }
        }

    }

    private fun updateSpeakerState() {
        val isSpeakerOn = CallManager.instance().isSpeakerOn
        if (isSpeakerOn) {
            CallManager.instance().setSpeakerOn(portSipSdk, false)
        } else {
            CallManager.instance().setSpeakerOn(portSipSdk, true)
        }
        btn_speaker?.isSelected = !isSpeakerOn
    }

    fun onCallDTFM(phone: String) {
        CallManager.instance().currentSession?.let { currentLine ->
            if (CallManager.instance().regist && currentLine.state == Session.CALL_STATE_FLAG.CONNECTED) {
                if (phone == "*") {
                    portSipSdk.sendDtmf(
                        currentLine.sessionID,
                        PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833,
                        10,
                        160,
                        true
                    )
                    return
                }

                if (phone == "#") {
                    portSipSdk.sendDtmf(
                        currentLine.sessionID,
                        PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833,
                        11,
                        160,
                        true
                    )
                    return
                }

                val sum: Int = phone.toInt() // 0~9
                portSipSdk.sendDtmf(
                    currentLine.sessionID,
                    PortSipEnumDefine.ENUM_DTMF_MOTHOD_RFC2833,
                    sum,
                    160,
                    true
                )
            }
        }

    }

    //handle call state
    private fun updateHoldState() {
        CallManager.instance().currentSession?.let { currentLine ->
            if ((currentLine.state != Session.CALL_STATE_FLAG.CONNECTED)) {
                return
            }

            if (currentLine.bHold) {

                val rt = portSipSdk.unHold(currentLine.sessionID)
                if (rt != 0) {
                    currentLine.bHold = false
                    return
                }

                currentLine.bHold = false

            } else {

                val rt = portSipSdk.hold(currentLine.sessionID)
                if (rt != 0) {
                    return
                }
                currentLine.bHold = true

            }
        }
    }

    private fun updateMuteState() {
        CallManager.instance().currentSession?.let { currentLine ->
            if (currentLine.bMute) {
                portSipSdk.muteSession(
                    currentLine.sessionID, false, false, false, false
                )
                currentLine.bMute = false

            } else {
                portSipSdk.muteSession(
                    currentLine.sessionID, false, true, false, true
                )
                currentLine.bMute = true

            }
        }

    }

    override fun onCallStateChanged(session: Session?, state: Session.CALL_STATE_FLAG) {
        performUIByCallState(state)
        when (session?.state) {
            Session.CALL_STATE_FLAG.CLOSED,
            Session.CALL_STATE_FLAG.FAILED -> {
                Log.d(LOG, session.lineName.toString() + ": Idle -- Kết thúc")
                session.bMute = false
                CallManager.instance().setSpeakerOn(portSipSdk, false)
            }
            Session.CALL_STATE_FLAG.CONNECTED -> {
                Log.d(
                    LOG,
                    session.lineName.toString() + ": CONNECTED + Đang trong cuộc gọi"
                )
            }
            Session.CALL_STATE_FLAG.INCOMING -> {
                Log.d(LOG, session.lineName.toString() + ": INCOMING")
            }
            Session.CALL_STATE_FLAG.TRYING -> {
                Log.d(LOG, session.lineName.toString() + ": TRYING")
            }
            else -> Unit
        }

        btnMute.isSelected = session?.bMute ?: false
        btn_speaker.isSelected = CallManager.instance().isSpeakerOn
    }

    protected fun performUIByCallState(state: Session.CALL_STATE_FLAG) {
        when (state) {
            Session.CALL_STATE_FLAG.INCOMING -> {
                tvDuration.text = "Cuộc gọi đến"
                btn_accept.visible()
                btn_decline.visible()
                setViewsEnable(
                    false,
                    btnMute,
                    btnHold,
                    btnForward
                )
            }
            Session.CALL_STATE_FLAG.TRYING -> {
                tvDuration.text = "Đang kết nối"
            }
            Session.CALL_STATE_FLAG.CONNECTED -> {
                btn_accept.gone()
                btn_decline.gone()
                btn_speaker.visible()
                btn_end_call_1.visible()
                setViewsEnable(
                    true,
                    btnMute,
                    btnHold,
                    btnForward
                )
                setViewsEnable(true, btnMute, btnHold, btnForward)
                //startDurationCounter()
            }
            Session.CALL_STATE_FLAG.FAILED -> {
                //stopDurationCounter()
                finishDelay()
                tvDuration.text = "Lỗi"
            }
            Session.CALL_STATE_FLAG.CLOSED -> {
                //stopDurationCounter()
                finishDelay()
                tvDuration.text = "Kết thúc"
            }
        }
    }

    private fun finishDelay() {
        Handler().postDelayed({
            finish()
        }, 1000)
    }

    fun setViewsEnable(isEnable: Boolean, vararg vs: View) {
        for (v in vs) {
            setViewEnable(isEnable, v, true)
        }
    }

    fun setViewEnable(
        isEnable: Boolean,
        v: View,
        animate: Boolean
    ) {
        if (animate) {
            ViewCompat.animate(v)
                .alpha(if (isEnable) 1.0f else 0.5f)
                .setDuration(300)
                .start()
        }
        v.isEnabled = isEnable
    }

    private fun performUIByType(type: CallType) {
        when (type) {
            CallType.TYPE_INCOMING_CALL -> {
                if (sessionID == -1) {
                    tvDuration.text = "Error"
                    finishDelay()
                    return
                }
                tvDuration.text = "Cuộc gọi đến"
                setViewsEnable(
                    false,
                    btnMute,
                    btnHold,
                    btnForward,
                    btn_speaker,
                    btn_end_call_1
                )
                setViewsEnable(
                    true, btn_accept, btn_decline
                )
            }
            CallType.TYPE_OUTGOING_CALL -> {
                tvDuration.text = "Đang kết nối"
                setViewsEnable(
                    false,
                    btnMute,
                    btnHold,
                    btnForward
                )
                setViewsEnable(true, btnMute, btn_end_call_1, btn_speaker)
                setViewsEnable(false, btnHold, btnForward, btn_accept, btn_decline)
            }
            else -> Unit
        }
    }

    companion object {
        const val PARAM_CALL_TYPE = "param_call_type"
        const val PARAM_OUT_NUMBER = "param_out_number"
        const val PARAM_SESSION_ID = "param_session_id"
        const val LOG = "CallActivity"
        fun launchIncomingCall(
            context: Context,
            sessionID: String
        ): Intent {
            return Intent(context, CallActivity::class.java).apply {
                this.putExtra(PARAM_CALL_TYPE, CallType.TYPE_INCOMING_CALL.rawValue)
                this.putExtra(PARAM_SESSION_ID, sessionID)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
        }

        fun launchOutgoingCall(
            context: Context,
            phoneNumber: String
        ): Intent {
            val i = Intent(context, CallActivity::class.java)
            i.putExtra(PARAM_CALL_TYPE, CallType.TYPE_OUTGOING_CALL.rawValue)
            i.putExtra(PARAM_OUT_NUMBER, phoneNumber)
            return i.also {
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
        }
    }

    enum class CallType(val rawValue: Int) {
        UNKNOWN(-1),
        TYPE_INCOMING_CALL(1),
        TYPE_OUTGOING_CALL(2);

        companion object {
            fun fromValue(v: Int): CallType {
                return values().firstOrNull { it.rawValue == v } ?: UNKNOWN
            }
        }
    }
}

private fun View.visible() {
    this.visibility = View.VISIBLE
}

private fun View.gone() {
    this.visibility = View.GONE
}
