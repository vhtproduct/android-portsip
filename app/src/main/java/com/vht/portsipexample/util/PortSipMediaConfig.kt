package com.vht.portsipexample.util

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.portsip.PortSipEnumDefine
import com.portsip.PortSipSdk

enum class PortSipMediaConfig(val rawValue: String) {
    MEDIA_ILBC("MEDIA_ILBC"),
    MEDIA_G722("MEDIA_G722"),
    MEDIA_PCMA("MEDIA_PCMA"),
    MEDIA_PCMU("MEDIA_PCMU"),
    MEDIA_G729("MEDIA_G729"),
    MEDIA_GSM("MEDIA_GSM"),
    MEDIA_AMR("MEDIA_AMR"),
    MEDIA_AMRWB("MEDIA_AMRWB"),
    MEDIA_SPEEX("MEDIA_SPEEX"),
    MEDIA_SPEEXWB("MEDIA_SPEEXWB"),
    MEDIA_ISACWB("MEDIA_ISACWB"),
    MEDIA_ISACSWB("MEDIA_ISACSWB"),
    MEDIA_OPUS("MEDIA_OPUS"),
    MEDIA_H264("MEDIA_H264"),
    MEDIA_VP8("MEDIA_VP8"),
    MEDIA_VP9("MEDIA_VP9"),
    MEDIA_AEC("MEDIA_AEC"),
    MEDIA_AGC("MEDIA_AGC"),
    MEDIA_CNG("MEDIA_CNG"),
    MEDIA_VAD("MEDIA_VAD"),
    MEDIA_ANS("MEDIA_ANS"),
}

enum class PSForwardConfig(val rawValue: String) {
    STR_FWOPENKEY("str_fwopenkey"),
    STR_FWBUSYKEY("str_fwbusykey"),
    STR_FWTOKEY("str_fwtokey"),
}

enum class CommonConfig(val rawValue: String) {

    STR_PRACKTITLE("str_pracktitle"),
    STR_RESOLUTION("str_resolution"),
}

fun configPreferences(context: Context, preferences: SharedPreferences, sdk: PortSipSdk?) {
    sdk!!.clearAudioCodec()
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_G722.rawValue, false)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_G722)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_PCMA.rawValue, true)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_PCMA)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_PCMU.rawValue, true)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_PCMU)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_G729.rawValue, true)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_G729)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_GSM.rawValue, false)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_GSM)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_ILBC.rawValue, false)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_ILBC)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_AMR.rawValue, false)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_AMR)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_AMRWB.rawValue, false)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_AMRWB)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_SPEEX.rawValue, false)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_SPEEX)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_SPEEXWB.rawValue, false)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_SPEEXWB)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_ISACWB.rawValue, false)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_ISACWB)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_ISACSWB.rawValue, false)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_ISACSWB)
    }
    //        if (preferences.getBoolean(context.getString(R.string.MEDIA_G7221), false)) {
//            sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_G7221);
//        }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_OPUS.rawValue, false)) {
        sdk.addAudioCodec(PortSipEnumDefine.ENUM_AUDIOCODEC_OPUS)
    }
    sdk.clearVideoCodec()
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_H264.rawValue, true)) {
        sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_H264)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_VP8.rawValue, true)) {
        sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_VP8)
    }
    if (preferences.getBoolean(PortSipMediaConfig.MEDIA_VP9.rawValue, true)) {
        sdk.addVideoCodec(PortSipEnumDefine.ENUM_VIDEOCODEC_VP9)
    }
    sdk.enableAEC(preferences.getBoolean(PortSipMediaConfig.MEDIA_AEC.rawValue, true))
    sdk.enableAGC(preferences.getBoolean(PortSipMediaConfig.MEDIA_AGC.rawValue, true))
    sdk.enableCNG(preferences.getBoolean(PortSipMediaConfig.MEDIA_CNG.rawValue, true))
    sdk.enableVAD(preferences.getBoolean(PortSipMediaConfig.MEDIA_VAD.rawValue, true))
    sdk.enableANS(preferences.getBoolean(PortSipMediaConfig.MEDIA_ANS.rawValue, false))
    val foward = preferences.getBoolean(PSForwardConfig.STR_FWOPENKEY.rawValue, false)
    val fowardBusy = preferences.getBoolean(PSForwardConfig.STR_FWBUSYKEY.rawValue, false)
    val fowardto = preferences.getString(PSForwardConfig.STR_FWTOKEY.rawValue, null)
    if (foward && !TextUtils.isEmpty(fowardto)) {
        sdk.enableCallForward(fowardBusy, fowardto)
    }
    sdk.enableReliableProvisional(preferences.getBoolean(CommonConfig.STR_PRACKTITLE.rawValue, false))
    val resolution = preferences.getString(CommonConfig.STR_RESOLUTION.rawValue, "CIF")
    var width = 352
    var height = 288
    if (resolution == "QCIF") {
        width = 176
        height = 144
    } else if (resolution == "CIF") {
        width = 352
        height = 288
    } else if (resolution == "VGA") {
        width = 640
        height = 480
    } else if (resolution == "720P") {
        width = 1280
        height = 720
    } else if (resolution == "1080P") {
        width = 1920
        height = 1080
    }
    sdk.setVideoResolution(width, height)
}