package com.vht.portsipexample.service

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.RingtoneManager
import android.os.Build
import android.os.IBinder
import android.os.PowerManager
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.portsip.OnPortSIPEvent
import com.portsip.PortSipEnumDefine
import com.portsip.PortSipErrorcode
import com.portsip.PortSipSdk
import com.vht.portsipexample.MainActivity
import com.vht.portsipexample.MainApplication
import com.vht.portsipexample.R
import com.vht.portsipexample.ui.CallActivity
import com.vht.portsipexample.util.*
import java.util.*

class PortSipService : Service(), OnPortSIPEvent, NetWorkReceiver.NetWorkListener {

    private val channelID = "PortSipService"
    private var mCpuLock: PowerManager.WakeLock? = null
    private val mEngine: PortSipSdk by lazy {
        MainApplication.portSipSDK
    }
    private var mNetWorkReceiver: NetWorkReceiver? = null
    private val appID: String
        get() = "com.vht.portsipexample"
    private var pushToken: String = ""
        get() = "//todo get"
    private val instanceID: String
        get() {
            val preferences = this.getSharedPreferences("portsip.config", Context.MODE_PRIVATE)

            var instanceID = preferences.getString(INSTANCE_ID, "")
            if (TextUtils.isEmpty(instanceID)) {
                instanceID = UUID.randomUUID().toString()
                preferences.edit().putString(INSTANCE_ID, instanceID).apply()
            }
            return instanceID!!
        }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        showServiceNotifyCation()
        initialSDK()
        registerReceiver()
        refreshPushToken()
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val result = super.onStartCommand(intent, flags, startId)
        if (ACTION_PUSH_MESSAGE == intent.action || ACTION_SIP_REGIEST == intent.action) {
            CallManager.instance().online = true
            if (CallManager.instance().regist) {
                mEngine.refreshRegistration(0)
            } else {
                registerToServer()
            }
        } else if (ACTION_SIP_UNREGIEST == intent.action) {
            CallManager.instance().online = false
            unregisterToServer()
        } else if (ACTION_SIP_REINIT == intent.action) {
            CallManager.instance().hangupAllCalls(mEngine)
            initialSDK()
        } else if (ACTION_PUSH_TOKEN == intent.action) {
            pushToken = intent.getStringExtra(EXTRA_PUSHTOKEN) ?: ""
            refreshPushToken()
        } else if (ACTION_REJECT_CALL == intent.action) {
            CallManager.instance().currentSession?.let { currentLine ->
                when (currentLine.state) {
                    Session.CALL_STATE_FLAG.INCOMING -> {
                        mEngine.rejectCall(currentLine.sessionID, 486)
                        onInviteClosed(sessionId = currentLine.sessionID)
                    }

                    Session.CALL_STATE_FLAG.CONNECTED,
                    Session.CALL_STATE_FLAG.TRYING -> Unit
                    else -> Unit
                }
                currentLine.reset()
            }
        }
        return result
    }

    private fun showServiceNotifyCation() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        val pendingIntent = PendingIntent.getActivity(
            this,
            0 /*requestCode*/,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        val builderNoti = NotificationCompat.Builder(this, channelID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("PortSipService")
            .setContentText("PortSipService")
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setAutoCancel(true)
        with(builderNoti) {
            priority = NotificationCompat.PRIORITY_DEFAULT
            setContentIntent(pendingIntent)
        }
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        with(notificationManager) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(channelID, "PortSipService", NotificationManager.IMPORTANCE_DEFAULT).apply {
                    description = "PortSipService"
                    vibrationPattern = longArrayOf(0L)
                }
                createNotificationChannel(channel)
            }
        }
        startForeground(SERVICE_NOTIFICATION, builderNoti.build())
    }

    private fun initialSDK() {
        mEngine.CreateCallManager(applicationContext)
        mEngine.setOnPortSIPEvent(this)
        val dataPath = getExternalFilesDir(null)?.absolutePath
        val certRoot = "$dataPath/certs"
        val preferences = this.getSharedPreferences("portsip.config", Context.MODE_PRIVATE)
        val rm = Random()
        val localPort = 5060 + rm.nextInt(60000)
        val transType = preferences.getInt(TRANS, 0)
        var result = mEngine.initialize(
            getTransType(transType), "0.0.0.0", localPort,
            PortSipEnumDefine.ENUM_LOG_LEVEL_DEBUG, dataPath,
            8, "PortSIP SDK for Android", 0, 0, certRoot, "", false, null
        )
        if (result != PortSipErrorcode.ECoreErrorNone) {
            showTipMessage("initialize failure ErrorCode = $result")
            CallManager.instance()!!.resetAll()
            return
        }
        result = mEngine.setLicenseKey("LicenseKey")
        if (result == PortSipErrorcode.ECoreWrongLicenseKey) {
            showTipMessage("The wrong license key was detected, please check with sales@portsip.com or support@portsip.com")
            return
        } else if (result == PortSipErrorcode.ECoreTrialVersionLicenseKey) {
            Log.w(
                "Trial Version",
                "This trial version SDK just allows short conversation, you can't hearing anything after 2-3 minutes, contact us: sales@portsip.com to buy official version."
            )
            showTipMessage("This Is Trial Version")
        }
        mEngine.setInstanceId(instanceID)
    }

    private fun refreshPushToken() {
        if (!TextUtils.isEmpty(pushToken)) {
            val pushMessage =
                "device-os=android;device-uid=$pushToken;allow-call-push=true;allow-message-push=true;app-id=$appID"
            //old version
            mEngine.addSipMessageHeader(-1, "REGISTER", 1, "portsip-push", pushMessage)
            //new version
            mEngine.addSipMessageHeader(-1, "REGISTER", 1, "x-p-push", pushMessage)
            mEngine.refreshRegistration(0)
        }
    }

    private fun registerToServer() {
        val preferences = this.getSharedPreferences("portsip.config", Context.MODE_PRIVATE)
        val srtType = preferences.getInt(SRTP, 0)
        val userName = preferences.getString(USER_NAME, "")
        val password = preferences.getString(USER_PWD, "")
        val displayName = preferences.getString(USER_DISPALYNAME, "")
        val authName = preferences.getString(USER_AUTHNAME, "")
        val userDomain = preferences.getString(USER_DOMAIN, "")
        val sipServer = preferences.getString(SVR_HOST, "")
        val serverPort = preferences.getString(SVR_PORT, "5060")
        val stunServer = preferences.getString(STUN_HOST, "")
        val stunPort = preferences.getString(STUN_PORT, "3478")
        val sipServerPort = serverPort!!.toInt()
        val stunServerPort = stunPort!!.toInt()
        mEngine.removeUser()
        var result = mEngine.setUser(
            userName, displayName, authName, password,
            userDomain, sipServer, sipServerPort, stunServer, stunServerPort, null, 5060
        )
        if (result != PortSipErrorcode.ECoreErrorNone) {
            showTipMessage("setUser failure ErrorCode = $result")
            CallManager.instance().resetAll()
            return
        }
        mEngine.setAudioDevice(PortSipEnumDefine.AudioDevice.SPEAKER_PHONE)
        mEngine.setVideoDeviceId(1)
        mEngine.setSrtpPolicy(srtType)
        configPreferences(this, preferences, mEngine)
        mEngine.enable3GppTags(false)
        if (!TextUtils.isEmpty(pushToken)) {
            val pushMessage =
                "device-os=android;device-uid=$pushToken;allow-call-push=true;allow-message-push=true;app-id=$appID"
            mEngine.addSipMessageHeader(-1, "REGISTER", 1, "portsip-push", pushMessage)
            //new version
            mEngine.addSipMessageHeader(-1, "REGISTER", 1, "x-p-push", pushMessage)
        }
        result = mEngine.registerServer(5, 5)
        if (result != PortSipErrorcode.ECoreErrorNone) {
            showTipMessage("registerServer failure ErrorCode =$result")
            mEngine.unRegisterServer()
            CallManager.instance().resetAll()
        }
    }

    private fun showTipMessage(tipMessage: String) {
        val broadIntent = Intent(REGISTER_CHANGE_ACTION)
        broadIntent.putExtra(EXTRA_REGISTER_STATE, tipMessage)
        sendPortSipMessage(tipMessage, broadIntent)
    }

    private fun getTransType(select: Int): Int {
        when (select) {
            0 -> return PortSipEnumDefine.ENUM_TRANSPORT_UDP
            1 -> return PortSipEnumDefine.ENUM_TRANSPORT_TLS
            2 -> return PortSipEnumDefine.ENUM_TRANSPORT_TCP
            3 -> return PortSipEnumDefine.ENUM_TRANSPORT_PERS_UDP
            4 -> return PortSipEnumDefine.ENUM_TRANSPORT_PERS_TCP
        }
        return PortSipEnumDefine.ENUM_TRANSPORT_UDP
    }

    fun unregisterToServerWithoutPush() {
        if (!TextUtils.isEmpty(pushToken)) {
            val pushMessage =
                "device-os=android;device-uid=$pushToken;allow-call-push=false;allow-message-push=false;app-id=$appID"
            mEngine.addSipMessageHeader(-1, "REGISTER", 1, "portsip-push", pushMessage)
            //new version
            mEngine.addSipMessageHeader(-1, "REGISTER", 1, "x-p-push", pushMessage)
        }
        mEngine.unRegisterServer()
        CallManager.instance().regist = false
    }

    private fun unregisterToServer() {
        mEngine.unRegisterServer()
        CallManager.instance().regist = false
        stopSelf()
    }

    //--------------------
    override fun onRegisterSuccess(statusText: String, statusCode: Int, sipMessage: String) {
        CallManager.instance().regist = true
        val broadIntent = Intent(REGISTER_CHANGE_ACTION)
        broadIntent.putExtra(EXTRA_REGISTER_STATE, statusText)
        broadIntent.putExtra(EXTRA_IS_ONLINE, true)
        sendPortSipMessage("onRegisterSuccess", broadIntent)
        keepCpuRun(true)
    }

    override fun onRegisterFailure(statusText: String, statusCode: Int, sipMessage: String) {
        val broadIntent = Intent(REGISTER_CHANGE_ACTION)
        broadIntent.putExtra(EXTRA_REGISTER_STATE, statusText)
        broadIntent.putExtra(EXTRA_IS_ONLINE, false)
        sendPortSipMessage("onRegisterFailure code: $statusCode\nmessage: $sipMessage", broadIntent)
        CallManager.instance().regist = false
        CallManager.instance().resetAll()
        keepCpuRun(false)
    }

    override fun onInviteIncoming(
        sessionId: Long,
        callerDisplayName: String,
        caller: String,
        calleeDisplayName: String,
        callee: String,
        audioCodecNames: String,
        videoCodecNames: String,
        existsAudio: Boolean,
        existsVideo: Boolean,
        sipMessage: String
    ) {

        if (CallManager.instance().findIncomingCall() != null) {
            mEngine.rejectCall(sessionId, 486) //busy
            return
        }
        val session: Session? = CallManager.instance().findIdleSession()
        var caller = caller.split("@".toRegex()).toTypedArray()[0]
        caller = caller.replaceFirst("(^(sip+(s)?+:))".toRegex(), "")
        session?.state = Session.CALL_STATE_FLAG.INCOMING
        session?.hasVideo = existsVideo
        session?.sessionID = sessionId
        session?.remote = caller
        session?.displayName = callerDisplayName.takeIf { it.isNotEmpty() } ?: caller

        val broadIntent = Intent(CALL_CHANGE_ACTION)
        broadIntent.putExtra(EXTRA_CALL_SEESIONID, sessionId)

        val description: String = session?.lineName.toString() + " onInviteIncoming"
        broadIntent.putExtra(EXTRA_CALL_DESCRIPTION, description)

        sendPortSipMessage(description, broadIntent)
        Ring.getInstance(this).startRingTone()
        val incomingIntent: Intent =
            CallActivity.launchIncomingCall(this, session?.sessionID.toString())
        startActivity(incomingIntent)
    }

    override fun onInviteTrying(sessionId: Long) {}
    override fun onInviteSessionProgress(
        sessionId: Long,
        audioCodecNames: String,
        videoCodecNames: String,
        existsEarlyMedia: Boolean,
        existsAudio: Boolean,
        existsVideo: Boolean,
        sipMessage: String
    ) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null) {
            session.bEarlyMedia = existsEarlyMedia
        }
    }

    override fun onInviteRinging(
        sessionId: Long,
        statusText: String,
        statusCode: Int,
        sipMessage: String
    ) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null && !session.bEarlyMedia) {
            Ring.getInstance(this).startRingBackTone()
        }
    }

    override fun onInviteAnswered(
        sessionId: Long,
        callerDisplayName: String,
        caller: String,
        calleeDisplayName: String,
        callee: String,
        audioCodecNames: String,
        videoCodecNames: String,
        existsAudio: Boolean,
        existsVideo: Boolean,
        sipMessage: String
    ) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null) {
            session.state = Session.CALL_STATE_FLAG.CONNECTED
            session.hasVideo = existsVideo
            val broadIntent = Intent(CALL_CHANGE_ACTION)
            broadIntent.putExtra(EXTRA_CALL_SEESIONID, sessionId)
            val description = session.lineName + " onInviteAnswered"
            broadIntent.putExtra(EXTRA_CALL_DESCRIPTION, description)
            sendPortSipMessage(description, broadIntent)
        }
        Ring.getInstance(this).stopRingBackTone()
    }

    override fun onInviteFailure(sessionId: Long, reason: String, code: Int, sipMessage: String) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null) {
            session.state = Session.CALL_STATE_FLAG.FAILED
            session.sessionID = sessionId
            val broadIntent = Intent(CALL_CHANGE_ACTION)
            broadIntent.putExtra(EXTRA_CALL_SEESIONID, sessionId)
            val description = session.lineName + " onInviteFailure" + reason
            broadIntent.putExtra(EXTRA_CALL_DESCRIPTION, description)
            sendPortSipMessage(description, broadIntent)
        }
        Ring.getInstance(this).stopRingBackTone()
    }

    override fun onInviteUpdated(
        sessionId: Long,
        audioCodecNames: String,
        videoCodecNames: String,
        existsAudio: Boolean,
        existsVideo: Boolean,
        sipMessage: String
    ) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null) {
            session.state = Session.CALL_STATE_FLAG.CONNECTED
            session.hasVideo = existsVideo
            val broadIntent = Intent(CALL_CHANGE_ACTION)
            broadIntent.putExtra(EXTRA_CALL_SEESIONID, sessionId)
            val description = session.lineName + " OnInviteUpdated"
            broadIntent.putExtra(EXTRA_CALL_DESCRIPTION, description)
            sendPortSipMessage(description, broadIntent)
        }
    }

    override fun onInviteConnected(sessionId: Long) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null) {
            session.state = Session.CALL_STATE_FLAG.CONNECTED
            session.sessionID = sessionId
            val broadIntent = Intent(CALL_CHANGE_ACTION)
            broadIntent.putExtra(EXTRA_CALL_SEESIONID, sessionId)
            val description = session.lineName + " OnInviteConnected"
            broadIntent.putExtra(EXTRA_CALL_DESCRIPTION, description)
            sendPortSipMessage(description, broadIntent)
            Ring.getInstance(this).stopRingTone() //stop ring
        }
        if (mEngine.audioDevices?.contains(PortSipEnumDefine.AudioDevice.BLUETOOTH) == true) {
            mEngine.setAudioDevice(PortSipEnumDefine.AudioDevice.BLUETOOTH)
        } else {
            CallManager.instance().setSpeakerOn(mEngine, CallManager.instance().isSpeakerOn)
        }
    }

    override fun onInviteBeginingForward(forwardTo: String) {}
    override fun onInviteClosed(sessionId: Long) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null) {
            session.state = Session.CALL_STATE_FLAG.CLOSED
            session.sessionID = sessionId
            val broadIntent = Intent(CALL_CHANGE_ACTION)
            broadIntent.putExtra(EXTRA_CALL_SEESIONID, sessionId)
            val description = session.lineName + " OnInviteClosed"
            broadIntent.putExtra(EXTRA_CALL_DESCRIPTION, description)
            sendPortSipMessage(description, broadIntent)
        }
        Ring.getInstance(this).stopRingTone()
    }

    override fun onDialogStateUpdated(
        BLFMonitoredUri: String,
        BLFDialogState: String,
        BLFDialogId: String,
        BLFDialogDirection: String
    ) {
        var text = "The user "
        text += BLFMonitoredUri
        text += " dialog state is updated: "
        text += BLFDialogState
        text += ", dialog id: "
        text += BLFDialogId
        text += ", direction: "
        text += BLFDialogDirection
    }

    override fun onRemoteUnHold(
        sessionId: Long,
        audioCodecNames: String,
        videoCodecNames: String,
        existsAudio: Boolean,
        existsVideo: Boolean
    ) {
    }

    override fun onRemoteHold(sessionId: Long) {}
    override fun onReceivedRefer(
        sessionId: Long,
        referId: Long,
        to: String,
        referFrom: String,
        referSipMessage: String
    ) {
    }

    override fun onReferAccepted(sessionId: Long) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null) {
            session.state = Session.CALL_STATE_FLAG.CLOSED
            session.sessionID = sessionId
            val broadIntent = Intent(CALL_CHANGE_ACTION)
            broadIntent.putExtra(EXTRA_CALL_SEESIONID, sessionId)
            val description = session.lineName + " onReferAccepted"
            broadIntent.putExtra(EXTRA_CALL_DESCRIPTION, description)
            sendPortSipMessage(description, broadIntent)
        }
        Ring.getInstance(this).stopRingTone()
    }

    override fun onReferRejected(sessionId: Long, reason: String, code: Int) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null) {
            session.state = Session.CALL_STATE_FLAG.CLOSED
            session.sessionID = sessionId
            val broadIntent = Intent(CALL_CHANGE_ACTION)
            broadIntent.putExtra(EXTRA_CALL_SEESIONID, sessionId)
            val description = session.lineName + " onReferRejected"
            broadIntent.putExtra(EXTRA_CALL_DESCRIPTION, description)
            sendPortSipMessage(description, broadIntent)
        }
        Ring.getInstance(this).stopRingTone()
    }

    override fun onTransferTrying(sessionId: Long) {}
    override fun onTransferRinging(sessionId: Long) {}
    override fun onACTVTransferSuccess(sessionId: Long) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null) {
            session.state = Session.CALL_STATE_FLAG.CLOSED
            session.sessionID = sessionId
            val broadIntent = Intent(CALL_CHANGE_ACTION)
            broadIntent.putExtra(EXTRA_CALL_SEESIONID, sessionId)
            val description = session.lineName + " Transfer succeeded, call closed"
            broadIntent.putExtra(EXTRA_CALL_DESCRIPTION, description)
            sendPortSipMessage(description, broadIntent)
            // Close the call after succeeded transfer the call
            mEngine.hangUp(sessionId)
        }
    }

    override fun onACTVTransferFailure(sessionId: Long, reason: String, code: Int) {
        val session = CallManager.instance().findSessionBySessionID(sessionId)
        if (session != null) {
            val broadIntent = Intent(CALL_CHANGE_ACTION)
            broadIntent.putExtra(EXTRA_CALL_SEESIONID, sessionId)
            val description = session.lineName + " Transfer failure!"
            broadIntent.putExtra(EXTRA_CALL_DESCRIPTION, "$description --- Reason: $reason ")
            sendPortSipMessage(description, broadIntent)
        }
    }

    override fun onReceivedSignaling(sessionId: Long, signaling: String) {
        Log.e("Signaling", "onReceivedSignaling $signaling")
    }

    override fun onSendingSignaling(sessionId: Long, signaling: String) {
        Log.e("Signaling", "onSendingSignaling $signaling")
    }

    override fun onWaitingVoiceMessage(
        messageAccount: String,
        urgentNewMessageCount: Int,
        urgentOldMessageCount: Int,
        newMessageCount: Int,
        oldMessageCount: Int
    ) {
    }

    override fun onWaitingFaxMessage(
        messageAccount: String,
        urgentNewMessageCount: Int,
        urgentOldMessageCount: Int,
        newMessageCount: Int,
        oldMessageCount: Int
    ) {
    }

    override fun onRecvDtmfTone(sessionId: Long, tone: Int) {}
    override fun onRecvOptions(optionsMessage: String) {}
    override fun onRecvInfo(infoMessage: String) {}
    override fun onRecvNotifyOfSubscription(
        sessionId: Long,
        notifyMessage: String,
        messageData: ByteArray,
        messageDataLength: Int
    ) {
    }

    //Receive a new subscribe
    override fun onPresenceRecvSubscribe(
        subscribeId: Long,
        fromDisplayName: String,
        from: String,
        subject: String
    ) {
        var contact: Contact? = ContactManager.instance().findContactBySipAddr(from)
        if (contact == null) {
            contact = Contact()
            contact.sipAddr = from
            ContactManager.instance().addContact(contact)
        }
        contact.subRequestDescription = subject
        contact.subId = subscribeId
        when (contact.state) {
            Contact.SUBSCRIBE_STATE_FLAG.ACCEPTED -> mEngine.presenceAcceptSubscribe(subscribeId)
            Contact.SUBSCRIBE_STATE_FLAG.REJECTED -> mEngine.presenceRejectSubscribe(subscribeId)
            Contact.SUBSCRIBE_STATE_FLAG.UNSETTLLED -> {
            }
            Contact.SUBSCRIBE_STATE_FLAG.UNSUBSCRIBE -> contact.state =
                Contact.SUBSCRIBE_STATE_FLAG.UNSETTLLED
        }
        val broadIntent = Intent(PRESENCE_CHANGE_ACTION)
        sendPortSipMessage("OnPresenceRecvSubscribe", broadIntent)
    }

    //update online status
    override fun onPresenceOnline(fromDisplayName: String, from: String, stateText: String) {
        val contact: Contact? = ContactManager.instance().findContactBySipAddr(from)
        contact?.subDescription = stateText
        val broadIntent = Intent(PRESENCE_CHANGE_ACTION)
        sendPortSipMessage("OnPresenceRecvSubscribe", broadIntent)
    }

    //update offline status
    override fun onPresenceOffline(fromDisplayName: String, from: String) {
        val contact: Contact? = ContactManager.instance().findContactBySipAddr(from)
        contact?.subDescription = "Offline"
        val broadIntent = Intent(PRESENCE_CHANGE_ACTION)
        sendPortSipMessage("OnPresenceRecvSubscribe", broadIntent)
    }

    override fun onRecvMessage(
        sessionId: Long,
        mimeType: String,
        subMimeType: String,
        messageData: ByteArray,
        messageDataLength: Int
    ) {
    }

    override fun onRecvOutOfDialogMessage(
        fromDisplayName: String,
        from: String,
        toDisplayName: String,
        to: String,
        mimeType: String,
        subMimeType: String,
        messageData: ByteArray,
        messageDataLengthsipMessage: Int,
        sipMessage: String
    ) {
        if ("text" == mimeType && "plain" == subMimeType) {
            Toast.makeText(
                this,
                "you have a mesaage from: " + from + "  " + String(messageData),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onSendMessageSuccess(sessionId: Long, messageId: Long) {}
    override fun onSendMessageFailure(
        sessionId: Long,
        messageId: Long,
        reason: String,
        code: Int
    ) {
    }

    override fun onSendOutOfDialogMessageSuccess(
        messageId: Long,
        fromDisplayName: String,
        from: String,
        toDisplayName: String,
        to: String
    ) {
    }

    override fun onSendOutOfDialogMessageFailure(
        messageId: Long,
        fromDisplayName: String,
        from: String,
        toDisplayName: String,
        to: String,
        reason: String,
        code: Int
    ) {
    }

    override fun onSubscriptionFailure(subscribeId: Long, statusCode: Int) {}
    override fun onSubscriptionTerminated(subscribeId: Long) {}
    override fun onPlayAudioFileFinished(sessionId: Long, fileName: String) {}
    override fun onPlayVideoFileFinished(sessionId: Long) {}
    override fun onReceivedRTPPacket(
        sessionId: Long,
        isAudio: Boolean,
        RTPPacket: ByteArray,
        packetSize: Int
    ) {
    }

    override fun onSendingRTPPacket(l: Long, b: Boolean, bytes: ByteArray, i: Int) {}
    override fun onAudioRawCallback(
        sessionId: Long,
        callbackType: Int,
        data: ByteArray,
        dataLength: Int,
        samplingFreqHz: Int
    ) {
    }

    override fun onVideoRawCallback(l: Long, i: Int, i1: Int, i2: Int, bytes: ByteArray, i3: Int) {}

    //--------------------
    private fun sendPortSipMessage(message: String?, broadIntent: Intent?) {
        Log.e(TAG, "tipMessage: $message")
        sendBroadcast(broadIntent)
    }

    @SuppressLint("WakelockTimeout")
    fun keepCpuRun(keepRun: Boolean) {
        val powerManager = getSystemService(Context.POWER_SERVICE) as PowerManager
        if (keepRun) { //open
            if (mCpuLock == null) {
                if (powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SipSample:CpuLock.")
                        .also { mCpuLock = it } == null
                ) {
                    return
                }
                mCpuLock!!.setReferenceCounted(false)
            }
            synchronized(mCpuLock!!) {
                if (!mCpuLock!!.isHeld) {
                    mCpuLock!!.acquire()
                }
            }
        } else { //close
            if (mCpuLock != null) {
                synchronized(mCpuLock!!) {
                    if (mCpuLock!!.isHeld) {
                        mCpuLock!!.release()
                    }
                }
            }
        }
    }

    override fun onNetworkChange(netMobile: Int) {
        if (netMobile == -1) {
            //invaluable
        } else {
            if (!CallManager.instance().regist) {
                CallManager.instance().online
                registerToServer()
                return
            }
            if (CallManager.instance().online) {
                mEngine.refreshRegistration(0)
            }
        }
    }

    private fun registerReceiver() {
        val filter = IntentFilter()
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        mNetWorkReceiver = NetWorkReceiver()
        mNetWorkReceiver?.listener = this
        registerReceiver(mNetWorkReceiver, filter)
    }

    private fun unregisterReceiver() {
        if (mNetWorkReceiver != null) {
            unregisterReceiver(mNetWorkReceiver)
        }
    }

    //String processName= getProcessName();||activityname.contains(processName)
    private val isForeground: Boolean
        get() {
            val activities: Array<String?> =
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
                    getActivePackages(this)
                } else {
                    getActivePackagesCompat(this)
                }
            if (activities.isNotEmpty()) {
                val packageName = packageName
                //String processName= getProcessName();||activityname.contains(processName)
                for (activityName in activities) {
                    if (activityName!!.contains(packageName)) {
                        return true
                    }
                }
                return false
            }
            return false
        }

    private fun getActivePackagesCompat(context: Context): Array<String?> {
        val mActivityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val taskInfo = mActivityManager.getRunningTasks(1)
        val componentName = taskInfo[0].topActivity
        val activePackages = arrayOfNulls<String>(1)
        activePackages[0] = componentName!!.packageName
        return activePackages
    }

    private fun getActivePackages(context: Context): Array<String?> {
        val activePackages: MutableSet<String?> = HashSet()
        val mActivityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val processInfoList = mActivityManager.runningAppProcesses
        for (processInfo in processInfoList) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                activePackages.addAll(listOf(*processInfo.pkgList))
            }
        }
        return activePackages.toTypedArray()
    }

    companion object {
        const val ACTION_SIP_REGIEST = "PortSip.AndroidSample.Test.REGIEST"
        const val ACTION_SIP_UNREGIEST = "PortSip.AndroidSample.Test.UNREGIEST"
        const val ACTION_SIP_REINIT = "PortSip.AndroidSample.Test.TrnsType"
        const val ACTION_REJECT_CALL = "PortSip.AndroidSample.Test.RejectCall"
        const val ACTION_PUSH_MESSAGE = "PortSip.AndroidSample.Test.PushMessageIncoming"
        const val ACTION_PUSH_TOKEN = "PortSip.AndroidSample.Test.PushToken"
        const val INSTANCE_ID = "instanceid"
        const val REGISTER_CHANGE_ACTION = "PortSip.AndroidSample.Test.RegisterStatusChagnge"
        const val CALL_CHANGE_ACTION = "PortSip.AndroidSample.Test.CallStatusChagnge"
        const val PRESENCE_CHANGE_ACTION = "PortSip.AndroidSample.Test.PRESENCEStatusChagnge"
        const val EXTRA_REGISTER_STATE = "RegisterStatus"
        const val EXTRA_CALL_SEESIONID = "SessionID"
        const val EXTRA_CALL_DESCRIPTION = "Description"
        const val EXTRA_PUSHTOKEN = "token"
        const val EXTRA_IS_ONLINE = "IsOnline"
        const val EXTRA_CALLID = "CALLID"
        const val TAG = "PortSipService"

        const val USER_NAME = "user_name"
        const val USER_PWD = "user_pwd"
        const val SVR_HOST = "svr_host"
        const val SVR_PORT = "svr_port"
        const val USER_DOMAIN = "user_domain"
        const val USER_DISPALYNAME = "user_dispalay"
        const val USER_AUTHNAME = "user_authname"
        const val STUN_HOST = "stun_host"
        const val STUN_PORT = "stun_port"
        const val TRANS = "trans_type"
        const val SRTP = "srtp_type"
        private const val SERVICE_NOTIFICATION = 31414
    }

}